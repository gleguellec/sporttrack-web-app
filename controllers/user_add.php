<?php
require_once('Controller.php');
require_once(MODEL_DIR.'/User.php');
require_once(MODEL_DIR.'/UserDAO.php');

/**
 * Class AddUserController extends Controller
 * This class is the controller used to display the add user page and to add the user
 */
class AddUserController extends Controller{
    public function get($request){
        $this->render('user_add',[]);
    }

    public function post($request){
        $user = new User();
        $user->init($request['prenom'],$request['nom'],$request['date'],$request['sexe'],$request['taille'],$request['poids'],$request['mail'],$request['password']);

        if (UserDAO::getInstance()->select($user->getEmail()) === null) { // if the user doesn't exist in the database
            UserDAO::getInstance()->insert($user);
            $this->render('user_add_valid',['success'=>true]);
        }
        else{
            $this->render('user_add_valid',['success'=>false]);
        }
    }
}
?>