<?php
require_once('Controller.php');

/**
 * Class DisconnectUserController extends Controller
 * This class is the controller used to disconnect the user
 */
class DisconnectUserController extends Controller{
    public function get($request){
        session_start();
        session_destroy(); // destroy the session to disconnect the user
        
        $this->render('disconnect',[]);
    }
}
?>