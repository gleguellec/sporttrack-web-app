<?php
require_once('Controller.php');

/**
 * Class AProposController extends Controller
 * This class is the controller used to display the about page
 */
class AProposController extends Controller{

    public function get($request){
        $this->render('apropos',[]);
    }
}

?>