<?php
require_once('Controller.php');

/**
 * Class ListActivityController extends Controller
 * This class is the controller used to display the list of activities
 */
class ListActivityController extends Controller{
    public function get($request){
        $this->render('activities',[]);
    }
}
?>