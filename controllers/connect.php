<?php
require_once('Controller.php');
require_once(MODEL_DIR.'/User.php');
require_once(MODEL_DIR.'/UserDAO.php');

/**
 * Class ConnectUserController extends Controller
 * This class is the controller used to display the connect page and to connect the user
 */
class ConnectUserController extends Controller{

    public function get($request){
        $this->render('connect',[]);
    }

    public function post($request){
        $foundUser = null;
        foreach (UserDAO::getInstance()->findAll() as $user) { // find the user in the database
            if ($user->getEmail() === $request['email'] && $user->getMotDePasse() === $request['password']) {
                $foundUser = $user;
            }
        }
        if ($foundUser !== null) { 
            session_start(); // start a session
            $_SESSION['user'] = $foundUser;

            $this->render('user_connect_valid', ['success' => true, 'user' => $foundUser]);
        } else {
            $this->render('user_connect_valid', ['success' => false]);
        }
    }
}

?>
