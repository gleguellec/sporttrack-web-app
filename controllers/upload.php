<?php
require_once('Controller.php');
require_once(MODEL_DIR . '/Activity.php');
require_once(MODEL_DIR . '/ActivityDAO.php');
require_once(MODEL_DIR . '/User.php');
require_once(MODEL_DIR . '/Data.php');
require_once(MODEL_DIR . '/DataDAO.php');

/**
 * Class UploadActivityController extends Controller
 * This class is the controller used to display the upload page and to upload a json file
 */
class UploadActivityController extends Controller{

    public function get($request){
        session_start();
        if (isset($_SESSION['user'])) {
            $this->render('upload', []);
        }
        else{
            $this->render('connect', []);
        }
    }

    public function post($request){
        if (isset($_FILES['file']) && $_FILES['file']['error'] === UPLOAD_ERR_OK) {

            // extract the data from the json file
            $dataJson = file_get_contents($_FILES['file']['tmp_name']);
            $data = json_decode($dataJson, true);

            // Parse JSON file
            $activityData = $data['activity'];
            $activityEntries = $data['data'];
            
            // Create Activity object
            $activity = new Activity();
            session_start();
            $activity->init($_SESSION['user']->getId(), $activityData['date'], $activityData['description']);
            
            // Save Activity object in database
            ActivityDAO::getInstance()->insert($activity);

            // Create ActivityEntry objects
            foreach ($activityEntries as $activityEntry) {
                $activityEntryObject = new Data();
                $activityEntryObject->init($activity->getId(), $activityEntry['time'], $activityEntry['cardio_frequency'], $activityEntry['latitude'], $activityEntry['longitude'], $activityEntry['altitude']);
                DataDAO::getInstance()->insert($activityEntryObject);
            }

            $this->render('upload', ['data' => $data]);
        }
        else{
            $this->render('upload', []);
        }
    }
}
?>