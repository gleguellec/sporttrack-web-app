<?php
require_once('Controller.php');

/**
 * Class MainController extends Controller
 * This class is the controller used to display the main page
 */
class MainController extends Controller{

    public function get($request){
        session_start();
        $isConnected = false;
        if(isset($_SESSION['user'])){ // check if the user is connected
            $isConnected = true;
        }
        $this->render('main',['isConnected' => $isConnected]); // render the main page with the isConnected variable
    }
}
?>