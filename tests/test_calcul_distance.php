<?php
define ("__ROOT__", dirname(dirname(__FILE__)));
require_once (__ROOT__.'/config.php');
require_once (MODEL_DIR.'/CalculDistanceImpl.php');

// Charger les données JSON depuis le fichier
$jsonData = file_get_contents("file.json");
$data = json_decode($jsonData, true);

// Créer une instance de CalculDistanceImpl
$calculDistance = new CalculDistanceImpl();

// Calculer la distance entre deux points GPS
// Accéder aux coordonnées du premier point
$lat1 = $data['data'][0]['latitude'];
$long1 = $data['data'][0]['longitude'];

// Accéder aux coordonnées du second point
$lat2 = $data['data'][1]['latitude'];
$long2 = $data['data'][1]['longitude'];

$distance2Points = $calculDistance->calculDistance2PointsGPS($lat1, $long1, $lat2, $long2);
echo "Distance entre les 2 points : " . $distance2Points . " mètres\n";

// Calculer la distance du parcours à partir des données JSON
$parcours = $data['data'];
$distanceTrajet = $calculDistance->calculDistanceTrajet($parcours);
echo "Distance du parcours IUT -> RU: " . $distanceTrajet . " mètres\n";
?>