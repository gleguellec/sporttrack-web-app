<?php
define ("__ROOT__", dirname(dirname(__FILE__)));
require_once (__ROOT__.'/config.php');
require_once (MODEL_DIR.'/User.php');
require_once (MODEL_DIR.'/UserDAO.php');
require_once (MODEL_DIR.'/Activity.php');
require_once (MODEL_DIR.'/ActivityDAO.php');
require_once (MODEL_DIR.'/Data.php');
require_once (MODEL_DIR.'/DataDAO.php');
require_once (MODEL_DIR.'/SqliteConnection.php');

$connection = new SqliteConnection();
$db = $connection->getConnection();

if ($db != null) {
    print("[Connection réussie]\n");

    // Test User
    $user = new User();
    print("============== Objet créé : \n");
    $user->init("Gwendal", "LE GUELLEC", "2000-01-01", "M", 170, 65, "email@test.com", "1234");
    print($user. "\n");

    $udao = new UserDAO();
    $udao->delete($user);
    $udao->insert($user);
    $udao->update($user);

    $users = $udao->findAll();

    print("============== Objet récupéré : \n");
    print($users[0]. "\n");
    
    // Test Activity
    $activity = new Activity();
    print("============== Objet créé : \n");
    $activity->init($user->getId(), "2021-01-01", "Course à pied");
    print($activity. "\n");

    $adao = new ActivityDAO();
    $adao->delete($activity);
    $adao->insert($activity);
    $adao->update($activity);

    $activities = $adao->findAll();

    print("============== Objet récupéré : \n");
    print($activities[0]. "\n");

    // Test Data
    $data = new Data();
    print("============== Objet créé : \n");
    $data->init($activity->getId(), "00:00:00", 0, 0.0, 0.0, 0);
    print($data. "\n");

    $ddao = new DataDAO();
    $ddao->delete($data);
    $ddao->insert($data);
    $ddao->update($data);

    $datas = $ddao->findAll();
    print("============== Objet récupéré : \n");
    print($datas[0]. "\n");

} else {
    echo 'Echec de la connexion';
}
?>