<?php include __ROOT__."/views/header.html"; ?>
    <div id="content">
    <div id="titre">
        <h2>Accueil</h2>
	    <p>Bienvenue sur la page d'accueil de l'application SportTrack.</p>
    </div>
    <div id="link">
        <?php 
        if (isset($isConnected)){
            if ($isConnected){
                echo "<a href='/disconnect'>Déconnexion</a><br>";
            }
            else{
                echo "<a href='/connect'>Connexion</a><br>";
            }
        }
        ?>
        <a href="/user_add">Créer mon compte</a><br>
        <a href="/apropos">A propos</a>
    </div>
</div>
<?php include __ROOT__."/views/footer.html"; ?>