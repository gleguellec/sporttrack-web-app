<?php
require_once(MODEL_DIR . '/ActivityDAO.php');
require_once(MODEL_DIR . '/Activity.php');
require_once(MODEL_DIR . '/DataDAO.php');
require_once(MODEL_DIR . '/Data.php');
require_once(MODEL_DIR . '/User.php');
require_once(MODEL_DIR . '/CalculDistanceImpl.php');

session_start();

if (isset($_SESSION['user'])) {
    // Initialiser une instance de CalculDistanceImpl
    $calculDistance = new CalculDistanceImpl();
    
    // récupérer dans la base de données les activités de l'utilisateur
    $activities = ActivityDAO::getInstance()->selectByUserId($_SESSION['user']->getId());    
    
    if (!empty($activities)) {
        // afficher les activités de l'utilisateur sous forme de tableau
        echo '<table class="activity-table">';
        echo '<thead>';
        echo '<tr>';
        echo '<th>Date</th>';
        echo '<th>Description</th>';
        echo '<th>Heure de début</th>';
        echo '<th>Fréquence cardiaque minimale</th>';
        echo '<th>Fréquence cardiaque maximale</th>';
        echo '<th>Fréquence cardiaque moyenne</th>';
        echo '<th>Distance parcourue (en mètres)</th>';
        echo '<th>Durée</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';
        
        foreach ($activities as $activity) {
            $dataOfActivity = DataDAO::getInstance()->selectByActivityId($activity->getId());
            if(!empty($dataOfActivity)){
                $freqCardiaqueArray = array();
                $latitudes = array();
                $longitudes = array();
                foreach ($dataOfActivity as $data) {
                    $freqCardiaqueArray[] = $data->getFreqCardiaque();
                    $latitudes[] = $data->getLatitude();
                    $longitudes[] = $data->getLongitude();
                }
                
                echo '<tr>';
                echo '<td>' . $activity->getDate() . '</td>';
                echo '<td>' . $activity->getDescription() . '</td>';
                
                // Afficher l'heure de début de l'activité (heure de la première donnée)
                $heureDebut = reset($dataOfActivity)->getHeure();
                echo '<td>' . $heureDebut . '</td>';
                
                // Calculer et afficher la fréquence cardiaque minimale
                $freqCardiaqueMin = min($freqCardiaqueArray);
                echo '<td>' . $freqCardiaqueMin . '</td>';
                
                // Calculer et afficher la fréquence cardiaque maximale
                $freqCardiaqueMax = max($freqCardiaqueArray);
                echo '<td>' . $freqCardiaqueMax . '</td>';
                
                // Calculer et afficher la fréquence cardiaque moyenne
                $freqCardiaqueMoy = array_sum($freqCardiaqueArray) / count($freqCardiaqueArray);
                echo '<td>' . round($freqCardiaqueMoy, 2) . '</td>';
                
                // Calculer et afficher la distance parcourue
                $parcours = array();
                for ($i = 0; $i < count($latitudes); $i++) {
                    $parcours[] = array(
                        'latitude' => $latitudes[$i],
                        'longitude' => $longitudes[$i]
                    );
                }
                $distanceParcourue = $calculDistance->calculDistanceTrajet($parcours);
                echo '<td>' . round($distanceParcourue, 2) . '</td>';
                
                // Calculer et afficher la durée
                $heureFin = end($dataOfActivity)->getHeure();
                $duree = strtotime($heureFin) - strtotime($heureDebut);
                echo '<td>' . formatDuration($duree) . '</td>';
                
                echo '</tr>';
            }
        }
        echo '</tbody>';
        echo '</table>';
    } else {
        echo "Vous n'avez pas encore d'activité";
    }
} else {
    echo "Vous n'êtes pas connecté";
}

function formatDuration($seconds) {
    $hours = floor($seconds / 3600);
    $minutes = floor(($seconds % 3600) / 60);
    $seconds = $seconds % 60;
    
    return sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
}
?>