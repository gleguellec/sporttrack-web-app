<?php include __ROOT__."/views/header.html"; ?>
	<div id="content">
        <div id="retour">
            <a href="/disconnect">&lt; Déconnexion</a>
        </div>
        <div id="titre">
		    <h2>Mes activités</h2>
            <?php include __ROOT__.'/views/list_activities.php';?>
        </div>
    </div>
<?php include __ROOT__."/views/footer.html"; ?>