<form action="/user_add" method="post">
    <input type="text" name="nom" id="nom" placeholder="Nom" required><br>
    <input type="text" name="prenom" id="prenom" placeholder="Prénom" required><br>
    <input type="date" name="date" id="date" required><br>
    <div id="sexe">
        <input type="radio" name="sexe" id="m" required><label for="m">Homme</label>
        <input type="radio" name="sexe" id="f" required><label for="f">Femme</label>        
    </div>          
    <input type="number" name="taille" id="taille" placeholder="Taille (en cm)" required><br>
    <input type="number" name="poids" id="poids" placeholder="Poids (en kg)" required><br>
    <input type="email" name="mail" id="mail" placeholder="E-mail" required><br>
    <input type="password" name="password" id="password" placeholder="Mot de passe" required pattern=".{8,}" title="Le mot de passe doit contenir 8 caractères ou plus"><br>
    <input type="submit" id="submit" value="Créer mon compte">
</form>