<?php include __ROOT__."/views/header.html"; ?>
	<div id="content">
      <div id="retour">
        <a href="/">&lt; Accueil</a>
    </div>
    <div id="titre">
		<h2>Connexion</h2>
	    <p>Entrez votre adresse mail et votre mot de passe.</p>
	</div>
    <?php include __ROOT__."/views/user_connect_form.php"; ?>
</div>
<?php include __ROOT__."/views/footer.html"; ?>