<?php include __ROOT__.'/views/header.html';?>
    <div id="content">
        <div id="retour">
            <a href="/">&lt; Accueil</a>
        </div>
        <div id="titre">            
            <h2>Création de compte</h2>
            <?php 
            if($data['success']){
                echo "Votre compte a bien été créé.";
                echo "<br/>Vous pouvez maintenant vous connecter.<br/><br/>";
                include __ROOT__.'/views/user_connect_form.php';
            }
            else{
                echo "Une erreur est survenue lors de la création de votre compte (e-mail déjà utilisé) <br/><br>";
                include __ROOT__.'/views/user_add_form.php';
            }
            ?>
        </div>
    </div>
<?php include __ROOT__.'/views/footer.html';?>