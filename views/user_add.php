<?php include __ROOT__.'/views/header.html';?>
    <div id="content">
        <div id="retour">
            <a href="/">&lt; Accueil</a>
        </div>
        <div id="titre">            
            <h2>Création de compte</h2>
            <p>Complétez le formulaire pour vous créer un compte SportTrack.</p>
        </div>
        <?php include __ROOT__.'/views/user_add_form.php';?>
    </div>
<?php include __ROOT__.'/views/footer.html';?>