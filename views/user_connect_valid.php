<?php include __ROOT__.'/views/header.html';?>
    <div id="content">
        <div id="retour">
            <a href="/disconnect">&lt; Déconnexion</a>
        </div>
        <div id="titre">            
            <h2>Mon compte</h2>
            <?php
            if($data['success']){
                echo "Vos informations sont les suivantes : <br/>";
                
                echo "Prénom : ".$data['user']->getPrenom()."<br/>";
                echo "Nom : ".$data['user']->getNom()."<br/>";
                echo "Date de naissance : ".$data['user']->getDateNaissance()."<br/>";
                echo "Sexe : ".$data['user']->getSexe()."<br/>";
                echo "Taille : ".$data['user']->getTaille()." cm<br/>";
                echo "Poids : ".$data['user']->getPoids()." kg<br/>";
                echo "E-mail : ".$data['user']->getEmail()."<br/>";
                echo "Mot de passe : ".$data['user']->getMotDePasse()."<br/><br/>";

                echo "<a href='upload'>Charger un fichier d'activité sportive</a><br/>";
                echo "<a href='activities'>Afficher mes activités</a>";
            }
            else{
                echo "E-mail ou mot de passe incorrect <br/><br>";
                include __ROOT__.'/views/user_connect_form.php';
            }
            ?>
        </div>
    </div>
<?php include __ROOT__."/views/footer.html";?>