<?php

/**
 * Class SqliteConnection
 * This class is used to connect to the database
 */
class SqliteConnection {

    private $db;
    private static SqliteConnection $instance;
    
    /**
     * SqliteConnection constructor.
     */
    public function __construct() {
        try{
            $this->db = new PDO('sqlite:'.DB_FILE);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e){
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
    }
    
    /**
     * This function returns the instance of the SqliteConnection
     */
    public static function getInstance(): SqliteConnection {
        if (!isset(self::$instance)) {
            self::$instance = new SqliteConnection();
        }
        return self::$instance;
    }

    /**
     * This function returns the connection to the database
     */
    public function getConnection(){
        return $this->db;
    }
}