<?php
interface CalculDistance {
    /**
     * Retourne la distance en mètres entre 2 points GPS exprimés en degrés.
     * @param float $lat1 Latitude du premier point GPS
     * @param float $long1 Longitude du premier point GPS
     * @param float $lat2 Latitude du second point GPS
     * @param float $long2 Longitude du second point GPS
     * @return float La distance entre les deux points GPS
     */
    public function calculDistance2PointsGPS(float $lat1, float $long1, float $lat2, float $long2): float;

    /**
     * Retourne la distance en mètres du parcours passé en paramètres. Le parcours est
     * défini par un tableau ordonné de points GPS.
     * @param Array $parcours Le tableau contenant les points GPS
     * @return float La distance du parcours
     */
    public function calculDistanceTrajet(Array $parcours): float;
}

/**
 * Class CalculDistanceImpl implements CalculDistance
 * Calculate distance between two points and distance of a path
 */
class CalculDistanceImpl implements CalculDistance {
    public function calculDistance2PointsGPS(float $lat1, float $long1, float $lat2, float $long2): float {
        $R = 6371000; // Rayon de la Terre en mètres
        $lat1 = deg2rad($lat1);
        $lat2 = deg2rad($lat2);
        $long1 = deg2rad($long1);
        $long2 = deg2rad($long2);
        $d = $R * acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($long1 - $long2));
        return $d;
    }

    public function calculDistanceTrajet(Array $parcours): float {
        $distance = 0;
        for ($i = 0; $i < count($parcours) - 1; $i++) {
            $distance += $this->calculDistance2PointsGPS($parcours[$i]['latitude'], $parcours[$i]['longitude'], $parcours[$i + 1]['latitude'], $parcours[$i + 1]['longitude']);
        }
        return $distance;
    }
}

?>