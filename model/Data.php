<?php
/**
 * Class Data
 */
class Data {

    private int $id;
    private int $idActivite;
    private string $heure;
    private int $freqCardiaque;
    private float $latitude;
    private float $longitude;
    private int $altitude;

    /**
     * Data constructor.
     */
    public function __construct() { }

    /**
     * This function initializes a data
     */
    public function init($a, $d, $f, $la, $lo, $alt): void {
        $this->id = -1;
        $this->idActivite = $a;
        $this->heure = $d;
        $this->freqCardiaque = $f;
        $this->latitude = $la;
        $this->longitude = $lo;
        $this->altitude = $alt;
    }

    // Getters and setters
    public function getId(int $id): void { $this->id; }
    public function getIdActivite(): int { return $this->idActivite; }
    public function getHeure(): string { return $this->heure; }
    public function getFreqCardiaque(): int { return $this->freqCardiaque; }
    public function getLatitude(): float { return $this->latitude; }
    public function getLongitude(): float { return $this->longitude; }
    public function getAltitude(): int { return $this->altitude; }

    public function setId($id): void { $this->id = $id; }

    /**
     * This function returns the data as a string
     */
    public function __toString(): string {
        $str = "Id : " . $this->id . "\n";
        $str .= "Id Activité : " . $this->idActivite . "\n";
        $str .= "Heure : " . $this->heure . "\n";
        $str .= "Fréquence Cardiaque : " . $this->freqCardiaque . "\n";
        $str .= "Latitude : " . $this->latitude . "\n";
        $str .= "Longitude : " . $this->longitude . "\n";
        $str .= "Altitude : " . $this->altitude . "\n";

        return $str;
    }
}