<?php

/**
 * Class Activity
 * This class represents an activity 
 */
class Activity{

    private int $id;
    private int $idUtilisateur;
    private string $date;
    private string $description;

    /**
     * Activity constructor.
     */
    public function  __construct() { }
    
    /**
     * This function initializes an activity
     */
    public function init($u, $d, $desc){
        $this->id = -1;
        $this->idUtilisateur = $u;
        $this->date = $d;
        $this->description = $desc;
    }

    // Getters and setters
    public function getId(): int { return $this->id; }
    public function getIdUtilisateur(): int { return $this->idUtilisateur; }
    public function getDate(): string { return $this->date; }
    public function getDescription(): string { return $this->description; }

    public function setId($id): void { $this->id = $id; }

    /**
     * This function returns the activity as a string
     */
    public function  __toString(): string {
        $str = "Id : ". $this->getId() ."\n";
        $str .= "Utilisateur (ID) : ". $this->getIdUtilisateur() ."\n";
        $str .= "Date : ". $this->getDate() ."\n";
        $str .= "Description : ". $this->getDescription() ."\n";
        return $str;
    }
}

?>