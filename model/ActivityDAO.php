<?php
require_once ('SqliteConnection.php');

/**
 * Class ActivityDAO
 * This class is the data access object of the Activity class
 */
class ActivityDAO {

    private static ActivityDAO $dao;

    /**
     * ActivityDAO constructor.
     */
    public function __construct() {
    }

    /**
     * This function returns the instance of the ActivityDAO
     */
    public static function getInstance(): ActivityDAO {
        if (!isset(self::$dao)) {
            self::$dao = new ActivityDAO();
        }
        return self::$dao;
    }

    /**
     * This function returns all the activities
     */
    public final function findAll(): Array{
        $dbc = SqliteConnection::getInstance()->getConnection();
        
        // prepare the SQL statement
        $query = "SELECT * FROM Activity ORDER BY id";
        $stmt = $dbc->query($query);

        // get the results
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Activity');
        return $results;
    }

    /**
     * This function inserts an activity in the database
     */
    public final function insert(Activity $st): void{
        if($st instanceof Activity){
            $dbc = SqliteConnection::getInstance()->getConnection();

            // prepare the SQL statement
            $query = "INSERT INTO Activity(idUtilisateur, date, description) VALUES (:idUtilisateur, :date, :description);";
            $stmt = $dbc->prepare($query);

            // bind the values
            $stmt->bindValue(':idUtilisateur', $st->getIdUtilisateur(), PDO::PARAM_INT);
            $stmt->bindValue(':date', $st->getDate(), PDO::PARAM_STR);
            $stmt->bindValue(':description', $st->getDescription(), PDO::PARAM_STR);

            // execute the prepared statement
            $stmt->execute();

            // update the id of the object with the id of the last inserted row
            $st->setId($dbc->lastInsertId());
        }
    }

    /**
     * This function deletes an activity from the database
     */
    public function delete(Activity $obj): void {
        if ($obj instanceof Activity) {
            $dbc = SqliteConnection::getInstance()->getConnection();

            // prepare the SQL statement
            $query = "DELETE FROM Activity WHERE idUtilisateur = :idUtilisateur AND date = :date AND description = :description;";
            $stmt = $dbc->prepare($query);

            // bind the values
            $stmt->bindValue(':idUtilisateur', $obj->getIdUtilisateur(), PDO::PARAM_INT);
            $stmt->bindValue(':date', $obj->getDate(), PDO::PARAM_STR);
            $stmt->bindValue(':description', $obj->getDescription(), PDO::PARAM_STR);

            // execute the prepared statement
            $stmt->execute();
        }
    }

    /**
     * This function updates an activity in the database
     */
    public function update(Activity $obj): void {
        if ($obj instanceof Activity) {
            $dbc = SqliteConnection::getInstance()->getConnection();

            // prepare the SQL statement
            $query = "UPDATE Activity SET idUtilisateur = :idUtilisateur, date = :date, description = :description WHERE date = :date AND description = :description;";
            $stmt = $dbc->prepare($query);

            // bind the values
            $stmt->bindValue(':idUtilisateur', $obj->getIdUtilisateur(), PDO::PARAM_INT);
            $stmt->bindValue(':date', $obj->getDate(), PDO::PARAM_STR);
            $stmt->bindValue(':description', $obj->getDescription(), PDO::PARAM_STR);

            // execute the prepared statement
            $stmt->execute();
        }
    }

    /**
     * This function selects an activity from the database
     */
    public function select($id): Activity {
        $dbc = SqliteConnection::getInstance()->getConnection();

        // prepare the SQL statement
        $query = "SELECT * FROM Activity WHERE id = '". $id. "';";
        $stmt = $dbc->query($query);

        // get the results
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Activity');
        return $results[0];
    }

    /**
     * This function selects all the activities of a user from the database
     */
    public function selectByUserId($id): Array {
        $dbc = SqliteConnection::getInstance()->getConnection();

        // prepare the SQL statement
        $query = "SELECT * FROM Activity WHERE idUtilisateur = '". $id. "';";
        $stmt = $dbc->query($query);

        // get the results
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Activity');
        return $results;
    }
}
?>