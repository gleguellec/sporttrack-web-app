<?php
/**
 * Class User
 */
class User{
    private int $id;
    private string $nom;
    private string $prenom;
    private string $dateNaissance;
    private string $sexe;
    private int $taille;
    private int $poids;
    private string $email;
    private string $motDePasse;

    /**
     * User constructor.
     */
    public function  __construct() { }

    /**
     * This function initializes a user
     */
    public function init($n, $p, $d, $s, $t, $po, $e, $m){
        $this->id = -1;
        $this->nom = $n;
        $this->prenom = $p;
        $this->dateNaissance = $d;
        $this->sexe = $s;
        $this->taille = $t;
        $this->poids = $po;
        $this->email = $e;
        $this->motDePasse = $m;
    }

    // Getters and setters
    public function getId(): int { return $this->id; }
    public function getNom(): string { return $this->nom; }
    public function getPrenom(): string { return $this->prenom; }
    public function getDateNaissance(): string { return $this->dateNaissance; }
    public function getSexe(): string { return $this->sexe; }
    public function getTaille(): int { return $this->taille; }
    public function getPoids(): int { return $this->poids; }
    public function getEmail(): string { return $this->email; }
    public function getMotDePasse(): string { return $this->motDePasse; }
    
    public function setId(int $id): void { $this->id = $id; }

    /**
     * This function returns the user as a string
     */
    public function  __toString(): string {
        $str = "Id: " . $this->id . "\n";
        $str .= "Nom: " . $this->nom . "\n";
        $str .= "Prénom: " . $this->prenom . "\n";
        $str .= "Date de Naissance: " . $this->dateNaissance . "\n";
        $str .= "Sexe: " . $this->sexe . "\n";
        $str .= "Taille: " . $this->taille . "\n";
        $str .= "Poids: " . $this->poids . "\n";
        $str .= "Email: " . $this->email . "\n";
        $str .= "Mot de Passe: " . $this->motDePasse . "\n";
    
        return $str;
    }
}
?>