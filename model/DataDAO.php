<?php
require_once('SqliteConnection.php');
require_once(MODEL_DIR . '/Data.php');

/**
 * Class DataDAO
 */
class DataDAO {

    private static DataDAO $dao;

    /**
     * DataDAO constructor.
     */
    public function __construct() {
    }

    /**
     * This function returns the instance of the DataDAO
     */
    public static function getInstance(): DataDAO {
        if (!isset(self::$dao)) {
            self::$dao = new DataDAO();
        }
        return self::$dao;
    }

    /**
     * This function returns all the data
     */
    public final function findAll(): Array{
        $dbc = SqliteConnection::getInstance()->getConnection();

        // prepare the SQL statement
        $query = "SELECT * FROM Data ORDER BY id;";
        $stmt = $dbc->query($query);

        // get the results
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Data');
        return $results;
    }

    /**
     * This function inserts a data in the database
     */
    public final function insert(Data $st): void{
        if($st instanceof Data){
            $dbc = SqliteConnection::getInstance()->getConnection();

            // prepare the SQL statement
            $query = "INSERT INTO Data(idActivite, heure, freqCardiaque, latitude, longitude, altitude) VALUES (:idActivite, :heure, :freqCardiaque, :latitude, :longitude, :altitude);";
            $stmt = $dbc->prepare($query);

            // bind the values
            $stmt->bindValue(':idActivite', $st->getIdActivite(), PDO::PARAM_INT);
            $stmt->bindValue(':heure', $st->getHeure(), PDO::PARAM_STR);
            $stmt->bindValue(':freqCardiaque', $st->getFreqCardiaque(), PDO::PARAM_INT);
            $stmt->bindValue(':latitude', $st->getLatitude(), PDO::PARAM_STR);
            $stmt->bindValue(':longitude', $st->getLongitude(), PDO::PARAM_STR);
            $stmt->bindValue(':altitude', $st->getAltitude(), PDO::PARAM_INT);

            // execute the prepared statement
            $stmt->execute();

            // update the id of the object with the id of the last inserted row
            $st->setId($dbc->lastInsertId());
        }
    }

    /**
     * This function deletes a data from the database
     */
    public function delete(Data $obj): void {
        if ($obj instanceof Data) {
            $dbc = SqliteConnection::getInstance()->getConnection();

            // prepare the SQL statement
            $query = "DELETE FROM Data WHERE heure = :heure AND freqCardiaque = :freqCardiaque AND latitude = :latitude AND longitude = :longitude AND altitude = :altitude;";
            $stmt = $dbc->prepare($query);

            // bind the values
            $stmt->bindValue(':heure', $obj->getHeure(), PDO::PARAM_STR);
            $stmt->bindValue(':freqCardiaque', $obj->getFreqCardiaque(), PDO::PARAM_INT);
            $stmt->bindValue(':latitude', $obj->getLatitude(), PDO::PARAM_STR);
            $stmt->bindValue(':longitude', $obj->getLongitude(), PDO::PARAM_STR);
            $stmt->bindValue(':altitude', $obj->getAltitude(), PDO::PARAM_INT);

            // execute the prepared statement
            $stmt->execute();
        }
    }

    /**
     * This function updates a data in the database
     */
    public function update(Data $obj): void {
        if ($obj instanceof Data) {
            $dbc = SqliteConnection::getInstance()->getConnection();

            // prepare the SQL statement
            $query = "UPDATE Data SET idActivite = :idActivite, heure = :heure, freqCardiaque = :freqCardiaque, latitude = :latitude, longitude = :longitude, altitude = :altitude WHERE id = :id;";
            $stmt = $dbc->prepare($query);

            // bind the values
            $stmt->bindValue(':idActivite', $obj->getIdActivite(), PDO::PARAM_INT);
            $stmt->bindValue(':heure', $obj->getHeure(), PDO::PARAM_STR);
            $stmt->bindValue(':freqCardiaque', $obj->getFreqCardiaque(), PDO::PARAM_INT);
            $stmt->bindValue(':latitude', $obj->getLatitude(), PDO::PARAM_STR);
            $stmt->bindValue(':longitude', $obj->getLongitude(), PDO::PARAM_STR);
            $stmt->bindValue(':altitude', $obj->getAltitude(), PDO::PARAM_INT);

            // execute the prepared statement
            $stmt->execute();
        }
    }

    /**
     * This function selects a data from the database
     */
    public function select($id): Data {
        $dbc = SqliteConnection::getInstance()->getConnection();

        // prepare the SQL statement
        $query = "SELECT * FROM Data WHERE id = :id";
        $stmt = $dbc->query($query);

        // bind the id to the PDO parameter
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        // get the results
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Data');
        return $results[0];
    }

    /**
     * This function selects all the data from an activity
     */
    public function selectByActivityId($id): Array {
        $dbc = SqliteConnection::getInstance()->getConnection();

        // prepare the SQL statement
        $query = "SELECT * FROM Data WHERE idActivite = '". $id. "';";
        $stmt = $dbc->query($query);

        // get the results
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Data');
        return $results;
    }
}

?>