<?php
require_once('SqliteConnection.php');

/**
 * Class UserDAO
 */
class UserDAO {

    private static UserDAO $dao;

    /**
     * UserDAO constructor.
     */
    public function __construct() {
    }

    /**
     * This function returns the instance of the UserDAO
     */
    public static function getInstance(): UserDAO {
        if (!isset(self::$dao)) {
            self::$dao = new UserDAO();
        }
        return self::$dao;
    }

    /**
     * This function returns all the users
     */
    public final function findAll(): Array{
        $dbc = SqliteConnection::getInstance()->getConnection();

        // prepare the SQL statement
        $query = "SELECT * FROM User ORDER BY email;";
        $stmt = $dbc->query($query);

        // get the results
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'User');
        return $results;
    }

    /**
     * This function inserts a user in the database
     */
    public final function insert(User $st): void{
        if($st instanceof User){
            $dbc = SqliteConnection::getInstance()->getConnection();

            // check if the User already exists in the database (based on email)
            $existingUser = $this->select($st->getEmail());
            print_r($existingUser);
            if ($existingUser === null) {
                    
                // prepare the SQL statement
                $query = "INSERT INTO User (nom, prenom, dateNaissance, sexe, taille, poids, email, motDePasse) VALUES (:nom, :prenom, :dateNaissance, :sexe, :taille, :poids, :email, :motDePasse);";
                $stmt = $dbc->prepare($query);
                
                // bind the values
                $stmt->bindValue(':nom', $st->getNom(), PDO::PARAM_STR);
                $stmt->bindValue(':prenom', $st->getPrenom(), PDO::PARAM_STR);
                $stmt->bindValue(':dateNaissance', $st->getDateNaissance(), PDO::PARAM_STR);
                $stmt->bindValue(':sexe', $st->getSexe(), PDO::PARAM_STR);
                $stmt->bindValue(':taille', $st->getTaille(), PDO::PARAM_INT);
                $stmt->bindValue(':poids', $st->getPoids(), PDO::PARAM_INT);
                $stmt->bindValue(':email', $st->getEmail(), PDO::PARAM_STR);
                $stmt->bindValue(':motDePasse', $st->getMotDePasse(), PDO::PARAM_STR);
                
                // execute the prepared statement
                $stmt->execute();
                
                // update the id of the object with the id of the last inserted row
                $st->setId($dbc->lastInsertId());
            }

            else {
                echo "User already exists";
            }
        }
    }

    /**
     * This function deletes a user from the database
     */
    public function delete(User $obj): void {
        if ($obj instanceof User) {
            $dbc = SqliteConnection::getInstance()->getConnection();

            // prepare the SQL statement
            $query = "DELETE FROM User WHERE nom = :nom AND prenom = :prenom;";
            $stmt = $dbc->prepare($query);

            // bind the values
            $stmt->bindValue(':nom', $obj->getNom(), PDO::PARAM_STR);
            $stmt->bindValue(':prenom', $obj->getPrenom(), PDO::PARAM_STR);

            // execute the prepared statement
            $stmt->execute();
        }
    }

    /**
     * This function updates a user in the database
     */
    public function update(User $obj): void {
        if ($obj instanceof User) {
            $dbc = SqliteConnection::getInstance()->getConnection();
        
            // prepare the SQL statement
            $query = "UPDATE User SET nom = :nom, prenom = :prenom, dateNaissance = :dateNaissance, sexe = :sexe, taille = :taille, poids = :poids, motDePasse = :motDePasse WHERE email = :email;";
            $stmt = $dbc->prepare($query);

            // bind the values
            $stmt->bindValue(':nom', $obj->getNom(), PDO::PARAM_STR);
            $stmt->bindValue(':prenom', $obj->getPrenom(), PDO::PARAM_STR);
            $stmt->bindValue(':dateNaissance', $obj->getDateNaissance(), PDO::PARAM_STR);
            $stmt->bindValue(':sexe', $obj->getSexe(), PDO::PARAM_STR);
            $stmt->bindValue(':taille', $obj->getTaille(), PDO::PARAM_INT);
            $stmt->bindValue(':poids', $obj->getPoids(), PDO::PARAM_INT);
            $stmt->bindValue(':email', $obj->getEmail(), PDO::PARAM_STR);
            $stmt->bindValue(':motDePasse', $obj->getMotDePasse(), PDO::PARAM_STR);

            // execute the prepared statement
            $stmt->execute();
        }
    }

    /**
     * This function selects a user from the database
     */
    public function select($email): ?User {
        $dbc = SqliteConnection::getInstance()->getConnection();
        
        // prepare the SQL statement
        $query = "SELECT * FROM User WHERE email = :email;";
        $stmt = $dbc->prepare($query);

        // bind the value
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);

        // execute the prepared statement
        $stmt->execute();
        
        // get the results
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'User');

        // return the selected object
        $ret = null;
        if (sizeof($results) > 0){
            $ret = $results[0];
        }
        return $ret;
    }
}
?>