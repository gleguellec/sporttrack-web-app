CREATE TABLE IF NOT EXISTS User(
    id INTEGER 
        CONSTRAINT pkUser PRIMARY KEY AUTOINCREMENT,
    nom TEXT(100) 
        CONSTRAINT nnUserNom NOT NULL,
    prenom TEXT(100) 
        CONSTRAINT nnUserPrénom NOT NULL,
    dateNaissance DATE
        CONSTRAINT nnUserDateNaissance NOT NULL,
    sexe TEXT(1) 
        CONSTRAINT nnUserSexe NOT NULL,
    taille INTEGER 
        CONSTRAINT nnUserTaille NOT NULL,
    poids INTEGER 
        CONSTRAINT nnUserPoids NOT NULL,
    email TEXT(100) 
        CONSTRAINT nnUserEmail NOT NULL
        CONSTRAINT ckUserEmail CHECK(email LIKE '%_@__%.__%')
        CONSTRAINT uqUserEmail UNIQUE,
    motDePasse TEXT(100) 
        CONSTRAINT nnUserPassword NOT NULL
);

CREATE TABLE IF NOT EXISTS Activity(
    id INTEGER
        CONSTRAINT pkActivity PRIMARY KEY AUTOINCREMENT,
    idUtilisateur INTEGER
        CONSTRAINT nnActivityUID NOT NULL
        CONSTRAINT fkActivityUser REFERENCES User(id),
    date DATE
        CONSTRAINT nnActivityDate NOT NULL,
    description TEXT(100)
        CONSTRAINT nnActivityDesc NOT NULL
    
    
);

CREATE TABLE IF NOT EXISTS Data(
    id INTEGER
        CONSTRAINT pkData PRIMARY KEY AUTOINCREMENT,
    idActivite INTEGER
        CONSTRAINT nnDataAID NOT NULL
        CONSTRAINT fkDataActivity REFERENCES Activity(id),
    heure TIME
        CONSTRAINT nnActivityHeure NOT NULL,
    freqCardiaque INTEGER
        CONSTRAINT nnActivityFreqCardiaque NOT NULL,
    latitude REAL
        CONSTRAINT nnActivityLat NOT NULL,
    longitude REAL
        CONSTRAINT nnActivityLong NOT NULL,
    altitude INTEGER
        CONSTRAINT nnActivityAlt NOT NULL
);